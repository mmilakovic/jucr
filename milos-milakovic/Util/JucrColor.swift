//
//  JucrColor.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 30.03.21.
//

import UIKit

extension UIColor {
    
    public class var primaryColor: UIColor {
        color(r: 232, g: 67, b: 96)
    }
    
    public class var secondaryBackground: UIColor {
        color(r: 245, g: 245, b: 245)
    }
    
    private class func color(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1.0)
    }

}
