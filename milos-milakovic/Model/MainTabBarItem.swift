//
//  MainTabBarItem.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//

import UIKit

enum MainTabBarItem: Int, CaseIterable, RawRepresentable {
    case charging = 0
    case search = 1
    case map = 2
    case profile = 3
    
    var image: UIImage? {
        switch self {
        case .charging:
            return UIImage(named: "tabbar.car")
        case .search:
            return UIImage(named: "tabbar.search")
        case .map:
            return UIImage(named: "tabbar.map")
        case .profile:
            return UIImage(named: "tabbar.profile")
        }
    }
    
    var viewController: UIViewController {
        switch self {
        case .charging:
            return UINavigationController(rootViewController: ChargingViewController())
        case .search, .map, .profile:
            return UIViewController()
        }
    }
}
