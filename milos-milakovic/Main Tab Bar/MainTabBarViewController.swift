//
//  MainTabBarViewController.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//
import UIKit

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        tabBar.tintColor = .red
        tabBar.isTranslucent = false
        viewControllers = MainTabBarItem.allCases.map({ (item) -> UIViewController in
            let viewController = item.viewController
            let tabBarItem = viewController.tabBarItem
            tabBarItem?.image = item.image
            return viewController
        })
    }
}
