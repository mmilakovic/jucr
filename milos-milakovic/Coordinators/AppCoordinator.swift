//
//  AppCoordinator.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = [Coordinator]()
    
    private weak var mainCoordinator: MainCoordinator?
    
    private weak var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
        self.navigationController = UINavigationController()
    }
    
    func start() {
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

extension AppCoordinator: Navigable {
    func navigate(_ route: Navigation) {
        switch route {
        case .main(let navi): showMainTabBar(state: navi)
        }
    }
}


private extension AppCoordinator {

    // MARK: - Main
    private func showMainTabBar(state: Navigation.Main) {
        if mainCoordinator == nil {
            let navigationController = UINavigationController()
            navigationController.isNavigationBarHidden = true
            let coordinator = MainCoordinator(navigationController: navigationController)
            coordinator.parentCoordinator = self
            addChild(coordinator)
            mainCoordinator = coordinator
        }
        window?.rootViewController = mainCoordinator?.navigationController
        window?.makeKeyAndVisible()
        mainCoordinator?.navigate(state)
    }
    
    private func addChild(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
        coordinator.start()
    }
}


protocol Coordinator: AnyObject {
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
}
