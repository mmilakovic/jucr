//
//  NavigationProtocol.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//

import Foundation

protocol Navigable {
    associatedtype Route
    func navigate(_ route: Route)
}

protocol NavigationProtocol {}
enum Navigation: NavigationProtocol {
    
    case main(Main)
    
    enum Main: NavigationProtocol {
        case charging
        case search
        case map
        case profile
    }
}
