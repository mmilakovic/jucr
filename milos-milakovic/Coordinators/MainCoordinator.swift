//
//  MainCoordinator.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//

import UIKit

final class MainCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    weak var parentCoordinator: AppCoordinator?
    var tabBarController: UITabBarController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let tabBarController = MainTabBarController()
        navigationController.pushViewController(tabBarController, animated: false)
        self.tabBarController = tabBarController
        
        let navController = tabBarController.viewControllers![MainTabBarItem.charging.rawValue] as! UINavigationController
        navController.isNavigationBarHidden = true
    }
}

extension MainCoordinator: Navigable {
    func navigate(_ route: Navigation.Main) {
        switch route {
        case .charging:
            tabBarController?.selectedIndex = MainTabBarItem.charging.rawValue
        case .search, .map, .profile:
            tabBarController?.selectedIndex = MainTabBarItem.charging.rawValue
        }
    }
    
    private func chargingNavigationController() -> UINavigationController {
        tabBarController?.viewControllers![MainTabBarItem.charging.rawValue] as! UINavigationController
    }
}
