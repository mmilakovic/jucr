//
//  CharginStationsTableView.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 31.03.21.
//

import Foundation
import UIKit

class ChargingStationsTableView: UITableView {
    
    private let NUMBER_OF_ITEMS = 10
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: .zero, style: .grouped)
        translatesAutoresizingMaskIntoConstraints = false
        tableFooterView = UIView()
        tableHeaderView = UIView()
        isScrollEnabled = false
        dataSource = self
        delegate = self
        backgroundColor = .clear
        showsVerticalScrollIndicator = false
        separatorStyle = .none
        register(ChargingStationTableViewCell.self, forCellReuseIdentifier: String(describing: ChargingStationTableViewCell.self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var height: CGFloat {
        ChargingStationTableViewCell.CELL_HEIGHT * CGFloat(NUMBER_OF_ITEMS) + 50 //additional space
    }
}

extension ChargingStationsTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        NUMBER_OF_ITEMS
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ChargingStationTableViewCell.self)) as! ChargingStationTableViewCell
        cell.setup()
        return cell
    }
}


extension ChargingStationsTableView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        ChargingStationTableViewCell.CELL_HEIGHT
    }
}
