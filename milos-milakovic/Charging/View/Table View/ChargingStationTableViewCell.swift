//
//  ChargingStationTableViewCell.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 31.03.21.
//

import Foundation
import UIKit

class ChargingStationTableViewCell: UITableViewCell {
    
    static let CELL_HEIGHT: CGFloat = 80.0
    
    private let contentsView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .secondaryBackground
        return view
    }()
    
    private let markerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "marker")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .gray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .left
        return label
    }()
    
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 11)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .gray
        label.textAlignment = .left
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        detailLabel.text = ""
    }
    
    public func setup() {
        selectionStyle = .none
        contentsView.layer.cornerRadius = 14.0
        titleLabel.text = "Berlin-Friedrichshain"
        detailLabel.text = "4/12 available"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addSubview(contentsView)
        [titleLabel, detailLabel, markerImageView].forEach{contentsView.addSubview($0)}
        NSLayoutConstraint.activate([
            contentsView.topAnchor.constraint(equalTo: topAnchor),
            contentsView.leadingAnchor.constraint(equalTo: leadingAnchor),
            contentsView.trailingAnchor.constraint(equalTo: trailingAnchor),
            contentsView.heightAnchor.constraint(equalToConstant: 60),
            
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            
            detailLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            detailLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            
            markerImageView.centerYAnchor.constraint(equalTo: contentsView.centerYAnchor),
            markerImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            markerImageView.widthAnchor.constraint(equalToConstant: 30),
            markerImageView.heightAnchor.constraint(equalToConstant: 30),
        ])
    }
}
