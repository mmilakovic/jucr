//
//  CollectionHeaderView.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 30.03.21.
//

import Foundation
import UIKit

class CollectionHeaderView: UIView {
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.text = "Statistics"
        return label
    }()
    
    private let dotsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "three.dots")
        return imageView
    }()
    
    convenience init(title: String) {
        self.init()
        titleLabel.text = title
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        [titleLabel, dotsImageView].forEach { addSubview($0) }
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            
            dotsImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            dotsImageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            dotsImageView.widthAnchor.constraint(equalToConstant: 20),
            dotsImageView.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
}
