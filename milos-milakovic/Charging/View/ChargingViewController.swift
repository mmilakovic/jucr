//
//  ChargingViewController.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 29.03.21.
//

import UIKit

class ChargingViewController: UIViewController {
    
    private let topView = ChargingHeaderView()
    private let colelctionHeaderView = CollectionHeaderView(title: "Statistics")
    private let tableViewHeaderView = CollectionHeaderView(title: "Nearby Supercharges")
    private let scrollView = UIScrollView()
    private let contentView = UIView()
    
    private let bottomView = ChargingStationsTableView()
    private var collapsed = false
    private let headerViewMaxHeight: CGFloat = 290
    private let headerViewMinHeight: CGFloat = 80 + UIApplication.shared.statusBarFrame.height
    private var topViewHeightContraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .primaryColor
        setupScrollView()
        setupViews()
        scrollView.delegate = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    func setupScrollView(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.backgroundColor = .white
        contentView.backgroundColor = .white
        scrollView.showsVerticalScrollIndicator = false
        
        view.addSubview(scrollView)
        view.addSubview(topView)
        scrollView.addSubview(contentView)
        
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        topViewHeightContraint = topView.heightAnchor.constraint(equalToConstant: 290)
        topViewHeightContraint.isActive = true
        
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
    }
    
    func setupViews(){
        contentView.addSubview(colelctionHeaderView)
        colelctionHeaderView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        colelctionHeaderView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        colelctionHeaderView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 9/10).isActive = true
        colelctionHeaderView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        contentView.addSubview(statisticsCollectionView)
        statisticsCollectionView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        statisticsCollectionView.topAnchor.constraint(equalTo: colelctionHeaderView.bottomAnchor, constant: 10).isActive = true
        statisticsCollectionView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 9/10).isActive = true
        statisticsCollectionView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        contentView.addSubview(tableViewHeaderView)
        tableViewHeaderView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        tableViewHeaderView.topAnchor.constraint(equalTo: statisticsCollectionView.bottomAnchor, constant: 25).isActive = true
        tableViewHeaderView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 9/10).isActive = true
        tableViewHeaderView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        contentView.addSubview(bottomView)
        bottomView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        bottomView.topAnchor.constraint(equalTo: tableViewHeaderView.bottomAnchor).isActive = true
        bottomView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        bottomView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        bottomView.heightAnchor.constraint(greaterThanOrEqualToConstant: bottomView.height).isActive = true
    }
    
    private let statisticsCollectionView: UICollectionView = {
        let view = StatisticsCollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        view.configure()
        return view
    }()
}

// MARK: - UIScrollViewDelegate
extension ChargingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        
        if y > 0 && !collapsed {
            topView.animateCollapse()
            collapsed = true
        } else if y < 0 {
            collapsed = false
            topView.animateExpand()
        }
        
        let newHeaderViewHeight: CGFloat = topViewHeightContraint!.constant - y
        if newHeaderViewHeight > headerViewMaxHeight {
            topViewHeightContraint!.constant = headerViewMaxHeight
        } else if newHeaderViewHeight < headerViewMinHeight {
            topViewHeightContraint!.constant = headerViewMinHeight
        } else {
            topViewHeightContraint!.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block s croll view
        }
    }
}
