//
//  StatisticsCollectionViewController.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 30.03.21.
//

import UIKit

class StatisticsCollectionView: UICollectionView {
    
    private let data = [
        StatisticsData(title: "240 V", subtitle: "Voltage", imageView: "car.battery"),
        StatisticsData(title: "540 km", subtitle: "Remaining", imageView: "battery.full"),
        StatisticsData(title: "20 min", subtitle: "Fast charge", imageView: "plugin")
    ]
    
    private let flowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumInteritemSpacing = Constants.minimumSpacing
        flowLayout.minimumLineSpacing = Constants.spacing
        flowLayout.sectionInset = UIEdgeInsets(top: Constants.collectionViewLayoutMargin, left: Constants.spacing, bottom: Constants.collectionViewLayoutMargin, right: Constants.spacing)
        return flowLayout
    }()
    
    func configure() {
        collectionViewLayout = flowLayout
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        showsHorizontalScrollIndicator = false
        register(StatisticsCollectionViewCell.self, forCellWithReuseIdentifier: String(describing: StatisticsCollectionViewCell.self))
        dataSource = self
        delegate = self
    }
}

extension StatisticsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: StatisticsCollectionViewCell.self), for: indexPath) as! StatisticsCollectionViewCell
        cell.setup(with: data[indexPath.row])
        return cell
    }
}

extension StatisticsCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}

private enum Constants {
    static let spacing: CGFloat = 20
    static let minimumSpacing: CGFloat = 30
    static let collectionViewLayoutMargin: CGFloat = 5.0
}
