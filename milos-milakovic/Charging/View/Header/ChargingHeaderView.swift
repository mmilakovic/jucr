//
//  ChargingHeaderView.swift
//  milos-milakovic
//
//  Created by Miloš Milaković on 30.03.21.
//

import Foundation
import UIKit

class ChargingHeaderView: UIView {
    
    private let headerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white.withAlphaComponent(0.7)
        label.font = .systemFont(ofSize: 13)
        label.text = "Good morning Billy"
        return label
    }()
    
    private let carNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .systemFont(ofSize: 17, weight: .bold)
        label.text = "Tesla Model X"
        label.alpha = 0.0
        return label
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .systemFont(ofSize: 23, weight: .bold)
        label.text = "Charging your car..."
        return label
    }()
    
    private let carImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "teslax")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private let chargingTimeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white.withAlphaComponent(0.7)
        label.font = .systemFont(ofSize: 13)
        
        let boldText = "49 MIN"
        let attrs = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .bold)]
        let attributedString = NSMutableAttributedString(string: boldText, attributes:attrs)
        
        let normalText = "TIME TO END OF CHARGE: "
        let normalString = NSMutableAttributedString(string:normalText)
        
        normalString.append(attributedString)
        label.attributedText = normalString
        return label
    }()
    
    private let percentageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .systemFont(ofSize: 21, weight: .bold)
        label.text = " 50% "
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .primaryColor
        layoutSubviews()
    }
    
    func animateExpand() {
        UIView.animate(withDuration: 0.5, animations: {
            self.carImageView.transform =  CGAffineTransform.identity
        })
        UIView.animate(withDuration: 0.2) {
            self.carNameLabel.alpha = 0.0
            self.headerLabel.alpha = 1.0
            self.titleLabel.alpha = 1.0
            self.chargingTimeLabel.alpha = 1.0
            self.percentageLabel.alpha = 1.0
        }
    }
    
    func animateCollapse() {
        let originalTransform = carImageView.transform
        let scaledTransform = originalTransform.scaledBy(x: 0.4, y: 0.4)
        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: carImageView.frame.origin.x + 130, y: 0)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.carImageView.transform = scaledAndTranslatedTransform
        })
        
        UIView.animate(withDuration: 0.2) {
            self.carNameLabel.alpha = 1.0
            self.headerLabel.alpha = 0.0
            self.titleLabel.alpha = 0.0
            self.chargingTimeLabel.alpha = 0.0
            self.percentageLabel.alpha = 0.0
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        percentageLabel.layer.borderWidth = 3
        percentageLabel.layer.borderColor = UIColor.white.cgColor
        percentageLabel.layer.cornerRadius = 0.5 * percentageLabel.bounds.size.width
        percentageLabel.layer.masksToBounds = true
        [carNameLabel, headerLabel, titleLabel, carImageView, chargingTimeLabel, percentageLabel].forEach { addSubview($0) }
        NSLayoutConstraint.activate([
            headerLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            headerLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: -10),
            
            carNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15),
            carNameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: carImageView.topAnchor, constant: -10),
            
            carImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            carImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            carImageView.widthAnchor.constraint(equalToConstant: 100),
            carImageView.heightAnchor.constraint(equalToConstant: 100),
            
            chargingTimeLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            chargingTimeLabel.topAnchor.constraint(equalTo: carImageView.bottomAnchor, constant: 5),
            
            percentageLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            percentageLabel.topAnchor.constraint(equalTo: chargingTimeLabel.bottomAnchor, constant: 5),
            percentageLabel.widthAnchor.constraint(equalTo: percentageLabel.heightAnchor)
        ])
    }
}
