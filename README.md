# README #

JUCRTestTask - Milos Milakovic

### Further Improvements ###

* View constraints are set by developer without values defined by designer
* Icons are downloaded from free icons website and sometimes don't support sizes for all iPhone sizes
* The circle view around % can be animated in order to show how much % of the battery is charged
* On statistics cells, on one of the provided screenshots, first view has shadow while on the other screenshot there's no such shadow. It's not added because I wasn't sure what is the idea behind it. Does it appear on click or should be always visible on the first cell? 
* Localization hasn't been taken into consideration (it's hardcoded now)
* Since there is no data loading and all the values are hardcoded, the architecture hasn't been taken into consideration. Otherwise MVVVM-C architecture would be implemented.
* System font has been used instead of Montserrat due to simplicity
* ![alt text](mask.png)
Not implemented because no appropriate image found to be added as a mask for the view. 


